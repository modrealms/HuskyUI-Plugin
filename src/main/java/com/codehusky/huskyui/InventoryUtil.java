package com.codehusky.huskyui;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.scheduler.Task;

public class InventoryUtil {
    public static void close(Player player) {
        Task.builder().execute(()->
            player.closeInventory()
        ).delayTicks(1).submit(HuskyUI.getInstance());
    }

    public static void open(Player player, Inventory inventory) {
        Task.builder().execute(()-> {
            player.openInventory(inventory);
        }).delayTicks(1).submit(HuskyUI.getInstance());
    }

    public static Slot getSlotFromInventory(Inventory inventory, int index){
        for(Inventory slot : inventory.slots()){
            if(!slot.getInventoryProperty(SlotIndex.class).isPresent()) continue;
            if(index == slot.getInventoryProperty(SlotIndex.class).get().getValue()){
                return (Slot) slot;
            }
        }
        return null;
    }
}
