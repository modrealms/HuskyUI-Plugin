package com.codehusky.huskyui.states.action;

public enum ClickType {
    PRIMARY,
    SECONDARY,
    SHIFT_PRIMARY,
    SHIFT_SECONDARY,
    INTERACT
}
