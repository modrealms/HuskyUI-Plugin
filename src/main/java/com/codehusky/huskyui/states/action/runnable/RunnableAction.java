/*
 * This file is part of HuskyUI.
 *
 * HuskyUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HuskyUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HuskyUI.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codehusky.huskyui.states.action.runnable;

import com.codehusky.huskyui.StateContainer;
import com.codehusky.huskyui.states.action.Action;
import com.codehusky.huskyui.states.action.ActionType;
import com.codehusky.huskyui.states.action.ClickType;
import com.codehusky.huskyui.states.element.ActionableElement;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.BiConsumer;

/**
 * A separate type of {@link Action} which performs developer-defined
 * actions separate from navigating the GUI.
 */
public class RunnableAction extends Action {

    /**
     * The UIRunnable that determines this action to be taken.
     */
    @Nullable private BiConsumer<ActionableElement, Slot> consumer;

    /**
     * Constructs a new RunnableAction without a pre-determined action.
     *
     * @param container the {@link StateContainer} responsible for this RunnableAction
     * @param actionType the type of {@link Action} being performed
     * @param goalState the destination for a {@link org.spongepowered.api.entity.living.player.Player}
     *                  after this Action is completed
     */
    public RunnableAction(@Nonnull final StateContainer container,
                          @Nonnull final ClickType clickType,
                          @Nonnull final ActionType actionType,
                          @Nonnull final String goalState) {
        this(container, clickType, actionType, goalState, null);
    }

    /**
     * Constructs a new RunnableAction with a pre-determined action.
     *
     * @param container the {@link StateContainer} responsible for this RunnableAction
     * @param actionType the type of {@link Action} being performed
     * @param goalState the destination for a {@link org.spongepowered.api.entity.living.player.Player}
     *                  after this Action is completed
     * @param consumer the additional Action to be performed
     */
    public RunnableAction(@Nonnull final StateContainer container,
                          @Nonnull final ClickType clickType,
                          @Nonnull final ActionType actionType,
                          @Nonnull final String goalState,
                          @Nullable final BiConsumer<ActionableElement, Slot> consumer) {
        super(container, clickType, actionType, goalState);
        this.consumer = consumer;
    }

    /**
     * Gets the additional action to be performed.
     *
     * @return the additional actions to be performed
     */
    @Nullable
    public BiConsumer<ActionableElement, Slot> getConsumer() {
        return this.consumer;
    }

    /**
     * Sets the additional actions to be performed.
     *
     * @param runnable the additional actions to be performed
     */
    public void setConsumer(@Nonnull final BiConsumer<ActionableElement, Slot> runnable) {
        this.consumer = runnable;
    }

    /**
     * Runs the additional actions.
     *
     * @param currentState the current State before the Action is performed
     */
    @Override
    public void runAction(@Nonnull final String currentState, Inventory inventory, ActionableElement aElement, Slot slot) {
        if (this.consumer != null) {
            this.consumer.accept(aElement, slot);
        } else {
            this.getObserver().sendMessage(Text.of(TextColors.RED, "Cannot run a null action!"));
        }
        super.runAction(currentState,inventory, aElement, slot);
    }

    /**
     * Creates a copy of this RunnableAction.
     *
     * @param newContainer the new {@link StateContainer} to be responsible for this new Action
     * @return a copy of this RunnableAction
     */
    @Nonnull
    @Override
    public RunnableAction copy(@Nonnull final StateContainer newContainer) {
        // UIRunnable doesn't need to be copied - it's just an action.
        return new RunnableAction(newContainer, this.getClickType(), this.getActionType(), this.getGoalState(), this.consumer);
    }
}
